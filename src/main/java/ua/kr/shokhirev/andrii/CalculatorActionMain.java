package ua.kr.shokhirev.andrii;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ua.kr.shokhirev.andrii.numbers.NumberOne;
import ua.kr.shokhirev.andrii.numbers.NumberTwo;
import ua.kr.shokhirev.andrii.numbers.OperationNumber;
import ua.kr.shokhirev.andrii.operations.ExecuteOperation;

import java.util.Scanner;

@Component
public class CalculatorActionMain
{
    public static void main( String[] args )
    {
        char[] operationsSignArray = {'+', '-', '*', '/'};
        int operationNumberInArray = 0;
        double numberOne = 0, numberTwo = 0;
        Scanner sc = new Scanner(System.in);
        char operationSign = ' ';

        NumberOne one = new NumberOne();
        NumberTwo two = new NumberTwo();

        OperationNumber operationNumber = new OperationNumber();

        while (true) {
            boolean isExistingOperationSign = false;
            System.out.print("Enter symbol of operation (+, -, *, /): ");

            operationSign = sc.next().charAt(0);

            for (operationNumberInArray = 0; operationNumberInArray < operationsSignArray.length; operationNumberInArray++) {
                if (Character.compare(operationSign, operationsSignArray[operationNumberInArray]) == 0) {
                    isExistingOperationSign = true;
                    break;
                }

            }
            if (!isExistingOperationSign) {
                System.out.println("Wrong operation sign. Try again!");
                continue;
            }
            break;
        }

        System.out.println("Now enter the two numbers on which you want to perform the operation");
        System.out.println("Example: 5 6,8 (use space between them): ");
        for (int counter = 0; counter < 2; counter++) {
            if (sc.hasNextDouble()) {
                if (counter == 0) {
                    double temp = sc.nextDouble();
                    one.setNumberOneValue(temp);
                    numberOne = temp;
                }
                if (counter == 1) {
                    double temp = sc.nextDouble();
                    two.setNumberTwoValue(temp);
                    numberTwo = temp;
                }
            }
        }
        operationNumber.setOperationNumber(operationNumberInArray);
        System.out.println("Expression is: " + numberOne + " " + operationSign + " " + numberTwo);

        ApplicationContext applicationContext =
               new AnnotationConfigApplicationContext(CalculatorActionMain.class.getPackageName());

        ExecuteOperation executeOperation = applicationContext.getBean(ExecuteOperation.class);

        double resultOfExpression = executeOperation.callNeededOperation();

        System.out.print("Result of expression " + numberOne + " " + operationSign + " " + numberTwo +
                " is ");
        System.out.printf("%.2f", resultOfExpression);
    }
}
