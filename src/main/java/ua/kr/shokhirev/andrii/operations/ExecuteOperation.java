package ua.kr.shokhirev.andrii.operations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.kr.shokhirev.andrii.numbers.NumberOne;
import ua.kr.shokhirev.andrii.numbers.NumberTwo;
import ua.kr.shokhirev.andrii.numbers.OperationNumber;

@Component
public class ExecuteOperation {
    @Autowired
    private NumberOne numberOne;
    @Autowired
    private NumberTwo numberTwo;
    @Autowired
    private OperationNumber operationNumber;
    @Autowired
    private AddOperation addOperation;
    @Autowired
    private SubOperation subOperation;
    @Autowired
    private DivOperation divOperation;
    @Autowired
    private MulOperation mulOperation;

    public double callNeededOperation() {
        if (operationNumber.getOperationNumber() == 0) {
            return addOperation.executeOperation(numberOne.getNumberOneValue(), numberTwo.getNumberTwoValue());
        }
        if (operationNumber.getOperationNumber() == 1) {
            return subOperation.executeOperation(numberOne.getNumberOneValue(), numberTwo.getNumberTwoValue());
        }
        if (operationNumber.getOperationNumber() == 2) {
            return mulOperation.executeOperation(numberOne.getNumberOneValue(), numberTwo.getNumberTwoValue());
        }
        if (operationNumber.getOperationNumber() == 3) {
            return divOperation.executeOperation(numberOne.getNumberOneValue(), numberTwo.getNumberTwoValue());
        }
        else throw new IllegalArgumentException("Operation number cannot be less than zero or greater than three");
    }


}
