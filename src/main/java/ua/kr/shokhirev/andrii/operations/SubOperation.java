package ua.kr.shokhirev.andrii.operations;

import org.springframework.stereotype.Component;

@Component
public class SubOperation implements Operation {

    @Override
    public double executeOperation(double numberOne, double numberTwo) {
        return numberOne - numberTwo;
    }
}
