package ua.kr.shokhirev.andrii.operations;

public interface Operation {
    public double executeOperation(double numberOne, double numberTwo);
}
