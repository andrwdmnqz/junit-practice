package ua.kr.shokhirev.andrii.numbers;

import org.springframework.stereotype.Component;

@Component
public class NumberTwo {
    private double numberTwoValue;

    public double getNumberTwoValue() {
        return numberTwoValue;
    }

    public void setNumberTwoValue(double numberOneValue) {
        this.numberTwoValue = numberOneValue;
    }
}