package ua.kr.shokhirev.andrii.numbers;

import org.springframework.stereotype.Component;

@Component
public class OperationNumber {
    private int operationNumber;

    public int getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(int operationNumber) {
        if (operationNumber < 0 || operationNumber > 3) {
            throw new IllegalArgumentException("Operation number cannot be less than zero or greater than three");
        }

        this.operationNumber = operationNumber;
    }
}
