package ua.kr.shokhirev.andrii.numbers;

import org.springframework.stereotype.Component;

@Component
public class NumberOne {
    private double numberOneValue;

    public double getNumberOneValue() {
        return numberOneValue;
    }

    public void setNumberOneValue(double numberOneValue) {
        this.numberOneValue = numberOneValue;
    }
}
