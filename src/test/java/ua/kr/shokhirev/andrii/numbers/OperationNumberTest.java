package ua.kr.shokhirev.andrii.numbers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OperationNumberTest {

    private OperationNumber operationNumber = new OperationNumber();

    @BeforeEach
    public void resetOperationNumber() {
        operationNumber.setOperationNumber(0);
    }

    @Test
    public void setOperationNumberInNormalConditions() {
        int expectedOperationNumber = 1;
        operationNumber.setOperationNumber(expectedOperationNumber);
        int realOperationNumber = operationNumber.getOperationNumber();
        Assertions.assertEquals(expectedOperationNumber, realOperationNumber, 0);
    }

    @Test
    public void negativeOperationNumberTest() {
        IllegalArgumentException lessThanZero = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            operationNumber.setOperationNumber(-5);
        });

        Assertions.assertEquals(IllegalArgumentException.class, lessThanZero.getClass());
    }

    @Test
    public void greaterThanThreeOperationNumberTest() {
        IllegalArgumentException greaterThanThree = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            operationNumber.setOperationNumber(5);
        });

        Assertions.assertEquals(IllegalArgumentException.class, greaterThanThree.getClass());
    }
}
