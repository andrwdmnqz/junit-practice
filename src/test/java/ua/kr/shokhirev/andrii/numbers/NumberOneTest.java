package ua.kr.shokhirev.andrii.numbers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NumberOneTest {

    private NumberOne numberOne = new NumberOne();

    @BeforeEach
    public void setNumber() {
        numberOne.setNumberOneValue(5);
    }

    @Test
    public void getNumberOneValueTest() {
        double expectedNumberOneValue = 5.0;
        double actualNumberOneValue = numberOne.getNumberOneValue();
        assertEquals(expectedNumberOneValue, actualNumberOneValue, 0.001);
    }

    @Test
    public void setNumberOneValueTest() {
        double newNumberOneValue = 10.0;
        numberOne.setNumberOneValue(newNumberOneValue);
        double updateNumberOneValue = numberOne.getNumberOneValue();
        assertEquals(newNumberOneValue, updateNumberOneValue, 0.001);
    }
}
