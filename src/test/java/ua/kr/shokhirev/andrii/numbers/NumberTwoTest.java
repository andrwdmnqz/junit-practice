package ua.kr.shokhirev.andrii.numbers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NumberTwoTest {

    private NumberTwo numberTwo = new NumberTwo();

    @BeforeEach
    public void setNumber() {
        numberTwo.setNumberTwoValue(10);
    }

    @Test
    public void setNumberTwoValueTest() {
        double newNumberTwoValue = 15;
        numberTwo.setNumberTwoValue(newNumberTwoValue);
        double numberTwoGetValue = numberTwo.getNumberTwoValue();
        assertEquals(newNumberTwoValue, numberTwoGetValue, 0.001);
    }

    @Test
    public void getNumberTwoValueTest() {
        double expectedNumberTwoValue = 10.0;
        double realNumberTwoValue = numberTwo.getNumberTwoValue();
        assertEquals(expectedNumberTwoValue, realNumberTwoValue, 0.001);
    }
}
