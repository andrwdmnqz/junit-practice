package ua.kr.shokhirev.andrii.operations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AddOperationTest {

    private AddOperation addOperation = new AddOperation();

    @Test
    public void addOperationInNormalConditions() {
        double expectedValue = 10.0;
        double realValue = addOperation.executeOperation(3.0, 7.0);
        assertEquals(expectedValue, realValue, 0.001);
    }

    @Test
    public void addOperationWithTwoNegatives() {
        double expectedValue = -10.0;
        double realValue = addOperation.executeOperation(-3.0, -7.0);
        assertEquals(expectedValue, realValue, 0.001);
    }
}
