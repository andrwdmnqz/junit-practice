package ua.kr.shokhirev.andrii.operations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DivOperationTest {

    private DivOperation divOperation = new DivOperation();

    @Test
    public void testDivOperationInNormalConditions() {
        double expectedDivValue = 2.0;
        double realDivValue = divOperation.executeOperation(10.0, 5.0);
        assertEquals(expectedDivValue, realDivValue, 0.001);
    }

    @Test()
    public void divOperationWithZeroDivisor() {
        IllegalArgumentException zeroDivisorValue = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            double realDivValue = divOperation.executeOperation(10.0, 0.0);
        });

        assertEquals(IllegalArgumentException.class, zeroDivisorValue.getClass());
    }
}
