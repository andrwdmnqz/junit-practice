package ua.kr.shokhirev.andrii.operations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SubOperationTest {

    private SubOperation subOperation = new SubOperation();

    @Test
    public void twoZeroNumbersSubTest() {
        double expectedValue = 0.0;
        double realValue = subOperation.executeOperation(0.0, 0.0);
        Assertions.assertEquals(expectedValue, realValue, 0.001);
    }

    @Test
    public void equalModuloDifferentSignsNumbers() {
        double expectedValue = -10.0;
        double realValue = subOperation.executeOperation(-5.0, 5.0);
        Assertions.assertEquals(expectedValue, realValue, 0.001);
    }
}
