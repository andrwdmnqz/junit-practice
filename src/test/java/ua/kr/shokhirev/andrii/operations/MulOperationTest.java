package ua.kr.shokhirev.andrii.operations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MulOperationTest {

    private MulOperation mulOperation = new MulOperation();

    @Test
    public void mulOperationInNormalConditions() {
        double expectedValue = 10.5;
        double realValue = mulOperation.executeOperation(5.0, 2.1);
        Assertions.assertEquals(expectedValue, realValue, 0.001);
    }

    @Test
    public void mulOperationWithTwoNegativeNumbers() {
        double expectedValue = 10.5;
        double realValue = mulOperation.executeOperation(-5.0, -2.1);
        Assertions.assertEquals(expectedValue, realValue, 0.001);
    }

    @Test
    public void mulOperationWithZeroNumber() {
        double expectedValue = 0.0;
        double realValue = mulOperation.executeOperation(-5.0, 0.0);
        Assertions.assertEquals(expectedValue, realValue, 0.001);
    }
}
